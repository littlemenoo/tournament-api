<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Player;
use App\Models\PlayerToRole;

class Role extends Model
{
    use HasFactory;

    protected $primaryKey = 'role_code';

    public function players()
    {
        return $this->belongsToMany(Player::class)->using(PlayerToRole::class);
    }
}
