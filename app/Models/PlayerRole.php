<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class PlayerRoles extends Pivot
{
    protected $table = "player_roles";
    use HasFactory;

    protected $fillable = [
        'player_id',
        'role_code'
    ];
}
