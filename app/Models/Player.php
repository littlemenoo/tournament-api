<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Role;
use App\Models\PlayerToRole;

class Player extends Model
{
    use HasFactory;
    protected $primaryKey = 'player_id';

    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'email',
        'team_code',
        'player_id',
        'role'
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class)->using(PlayerToRole::class);
    }
}
