<?php

namespace App\Models;

use DB;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'name',
        'team_id',
    ];

    protected $primaryKey = 'team_id';

    public $incrementing = false;
    protected $keyType = 'string';

    public static function getStandings()
    {
        $query=<<<EOT
select `code`, `points`.`team_code`, `name`, SUM(points.points) as total_points 
from `teams` 
left join `points` 
on `teams`.`code` = `points`.`team_code` 
group by `teams`.`name`
EOT;

         return $standings = DB::select($query)->get();
     }
}
