<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Player;
use App\Models\PlayerRole;
use Exception;
use Validator;
use Illuminate\Validation\Rule;
use DB;

class PlayersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $players = DB::table('players')
                        ->leftJoin('teams', 'players.team_code', '=', 'teams.code')
                        ->select('teams.name as team_name', 'username', 'first_name', 'last_name')
                        ->get();
        return $players;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = array(

            'role'=>[
                'required',
                 Rule::in(['JUNGLER', 'TANK', 'MIDLANER', 'TURTLE-LANE', 'NON-TURTLE-LANE']),
             ],
             
            'email' => 'required|email',
            'username' => 'required|unique:players|max:255',
            'first_name' => 'required',
            'last_name' => 'required'
        );
        
        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
            return $validator->errors();
        } else {
            $player = new Player;
            $uniqCode = Str::random(9);

            $player->team_code = $request->team_code;
            $player->username = $request->username;
            $player->first_name = $request->first_name;
            $player->last_name = $request->last_name;
            $player->email = $request->email;
            $player->player_id =  'pl_'.$uniqCode;
            $player->role =  $request->role;

            $save_player = $player->create([
                'team_code' => $player->team_code,
                'username' => $player->username,
                'first_name' => $player->first_name,
                'last_name' => $player->last_name,
                'email' => $player->email,
                'player_id' => $player->player_id,
                'role' => $player->role,
            ]);

            return $save_player;
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $player = Player::where('player_id', $id)->first();

        $input = $request->all();
        $player->fill($input)->save();

        return $player;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $player = Player::where('player_id', $id)->first();
        $player->delete();
    }
}
