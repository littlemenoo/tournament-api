<?php

namespace App\Http\Controllers;

use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Http\Request;
use App\Models\Team;
use Illuminate\Support\Str;
use DB;
use App\Models\Point;

class TeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        if(strlen($request->search) > 0) {
            $ranks = DB::table('teams')
                        ->leftJoin('points', 'teams.code', '=', 'points.team_code')
                        ->select('name', DB::raw('SUM(points.points) as total_points'), DB::raw('ROW_NUMBER() OVER(ORDER BY total_points DESC) AS rank'))
                        ->groupBy('points.team_code')
                        ->orderBy('total_points', 'DESC')->get()->toArray();   

            // $final_rank = $ranks->where('name', '=', $search)->get();
            
            $neededObject = array_filter(
                $ranks,
                function ($e) use (&$search) {
                    return $e->name == $search;
                }
            );

            return $neededObject;
        } else {
            $teams = Team::all();
            return $teams;
        }
        
    }

    public function standings()
    {
        
        $teams = new Team;

        // $ranks = [];

        $ranks = DB::table('teams')
                        ->leftJoin('points', 'teams.code', '=', 'points.team_code')
                        ->select('name', DB::raw('SUM(points.points) as total_points'), DB::raw('ROW_NUMBER() OVER(ORDER BY total_points DESC) AS rank'))
                        ->groupBy('points.team_code')
                        ->orderBy('total_points', 'DESC')
                        ->get();         
        return $ranks;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $team = new Team;
        $uniqCode = Str::random(9);

        $team->name = $request->name;
        $team->code =  $request->name . '_' . $uniqCode;
        $team->team_id =  'tm_' . $uniqCode;

        // $team->save();

        $save_team = $team->create([
            'name' => $team->name,
            'code' => $team->code,
            'team_id' => $team->team_id,
        ]);

        return $save_team;
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
			$team = Team::where('team_id', $id)->first();

			return $team;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $team = Team::where('team_id', $id)->first();

        $affected_team = DB::table('teams')
					->where('team_id', $team->team_id)
					->update(['name' => $request->name]);


		return $team;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $team = Team::where('team_id', $id)->first();

        DB::table('teams')->where('team_id', $team->team_id)->delete();
    }
}
