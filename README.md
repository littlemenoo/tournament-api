## Add One Team / POST Method
url: https://arcane-badlands-34504.herokuapp.com/api/teams

```
Request body
{
    "name": <team name>
}
```

## Search Teams / GET Method

url: https://arcane-badlands-34504.herokuapp.com/api/teams

```
Request body
{
    "search": <team name>
}
```

## View All Teams / GET Method

url: https://arcane-badlands-34504.herokuapp.com/api/teams

## Edit One Team / PUT/PATCH Method
url: https://arcane-badlands-34504.herokuapp.com/api/teams/{team_id}

```
Request body
{
    "name": <updated team name>
}
```

## Delete One Team / DELETE Method

url: https://arcane-badlands-34504.herokuapp.com/api/teams/{team_id}

## View One Team / VIEW Method

url: https://arcane-badlands-34504.herokuapp.com/api/teams/{team_id}

## Search Teams / GET Method

url: https://arcane-badlands-34504.herokuapp.com/api/teams

```
Request body
{
    "search": <team name>
}
```


## Add point Teams / POST Method

url: https://arcane-badlands-34504.herokuapp.com/api/teams

```
Request body
{
    "points": <points>,
    "team_code": <team_code>
}
```

## Teams Rankings / GET Method

url: https://arcane-badlands-34504.herokuapp.com/api/standings


## Add One Player / POST Method
url: https://arcane-badlands-34504.herokuapp.com/api/players

```
Request body
{
    "email": <email>,
    "username": <username>,
    "first_name": <first_name>,
    "last_name": <last_name>,
    "team_code": <team_code>,
    "role": <role>
}
```

## View All Players / GET Method

url: https://arcane-badlands-34504.herokuapp.com/api/players

## Edit One Player / PUT/PATCH Method
url: https://arcane-badlands-34504.herokuapp.com/api/players/{player_id}

```
Request body
{
    "email": <updated_email>,
    "username": <updated_username>,
    "first_name": <updated_first_name>,
    "last_name": <updated_last_name>,
    "team_code": <updated_team_code>,
    "role": <updated_role>
}
```

## Delete One Player / DELETE Method

url: https://arcane-badlands-34504.herokuapp.com/api/players/{player_id}

## View One Player / VIEW Method

url: https://arcane-badlands-34504.herokuapp.com/api/players/{player_id}